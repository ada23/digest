with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ;

package body digest_cli is                          -- [cli/$_cli]


    procedure ProcessCommandLine is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin
        GNAT.Command_Line.Define_Switch (Config,
                       verbose'access ,
                       Switch => "-v?",
                       Long_Switch => "--verbose?",
                       Help => "Output extra verbose information");
        GNAT.Command_Line.Define_Switch (Config,
                                      output => md5_alg'access,
                                      Switch => "-m",
                                      Long_Switch => "--md5",
                                      Help => "Use md5 algorithm");
        GNAT.Command_Line.Define_Switch (Config,
                                         output => sha_level'access,
                                         Switch => "-s=",
                                         Long_Switch => "--sha=",
                                         Help => "Use sha algorithm 1|224|256|384|512" );
        GNAT.Command_Line.Define_Switch (Config,
                                         output => crc_alg'access,
                                         Switch => "-c",
                                         Long_Switch => "--crc",
                                         Help => "Use crc" );

        GNAT.Command_Line.Define_Switch( Config ,
                                         output => Recursive'access ,
                                         Switch => "-r" ,
                                         Long_Switch => "--recursive" ,
                                         Help => "Recurse into the provided directory") ;
        GNAT.Command_Line.Define_Switch( Config ,
                                         Switch => "-p!" ,
                                         Long_Switch => "--pattern!" ,
                                         Help => "File name pattern to search for. Example -p*.adb ") ;

        GNAT.Command_Line.GetOpt(Config);

        if sha_level > 0
        then
            sha_alg := true ;
        end if;

    end ProcessCommandLine;

   function GetNextArgument return String is
   begin
      return GNAT.Command_Line.Get_Argument(Do_Expansion => True) ;
   end GetNextArgument ;

   procedure Show_Version is
   begin
      Put(Version); Put(" ");
      Put(GNAT.Source_Info.Compilation_Date); Put(" "); Put(GNAT.Source_Info.Compilation_Time);
      New_Line;
   end Show_Version ;



end digest_cli ;                                   -- [cli/$_cli]
