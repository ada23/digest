with System ;
with Interfaces.C ;
with Ada.Streams ;
with Ada.Streams.Stream_Io ;
with Ada.Text_Io ; use Ada.Text_Io ;
with GNAT.MD5 ;
with GNAT.SHA1 ;
with GNAT.SHA224 ;
with GNAT.SHA256 ;
with GNAT.SHA384 ;
with GNAT.SHA512 ;
with GNAT.CRC32 ;
with Hex ;
with dirwalk ;

package body digest_pkg is

   procedure dirwalk_md5 is new dirwalk( integer ) ;
   procedure dirwalk_sha is new dirwalk( integer ) ;
   procedure Dirwalk_Crc32 is new Dirwalk( Integer ) ;
   procedure Dirwalk_Adler32 is new Dirwalk( Integer ) ;
   
   function digest_md5( filename : string ) return String is
       use Ada.Streams ;
       f : ada.Streams.Stream_IO.File_Type ;
       buffer : ada.Streams.Stream_Element_Array(1..BLOCKSIZE) ;
       bufbytes : ada.Streams.Stream_Element_Count ;
       bytes : ada.Streams.Stream_Element_Count := 0 ;
       C : Gnat.MD5.Context := Gnat.Md5.Initial_Context ;
    begin
       ada.Streams.stream_io.Open(f , ada.streams.Stream_IO.In_File , filename ) ;
       while not ada.Streams.Stream_IO.End_Of_File(f)
       loop
          ada.Streams.stream_io.Read(f,buffer,bufbytes) ;
          Gnat.MD5.Update(C,Buffer(1..Bufbytes)) ;
          bytes := bytes + bufbytes ;
       end loop ;
       ada.Streams.Stream_IO.Close(f) ;
       return Gnat.MD5.Digest(C) ;
        exception
           when others =>
              put("Unable to generate md5 digest for ");
              put_line(filename) ;
              return "" ;
    end digest_md5 ;
    procedure digest_md5( context : integer ; filename : string ) is
    begin
      put( digest_md5(filename)) ;
      Put( " : ");
      put_line( filename );
    end digest_md5 ;
    procedure digest_md5( dirname : string ;
                          pattern : string ) is
    begin
        dirwalk_md5( 0 , dirname , pattern , digest_md5'access) ;
    end digest_md5 ;

    function digest_sha1( filename : string ) return String is
        use Ada.Streams ;
        f : ada.Streams.Stream_IO.File_Type ;
        buffer : ada.Streams.Stream_Element_Array(1..BLOCKSIZE) ;
        bufbytes : ada.Streams.Stream_Element_Count ;
        bytes : ada.Streams.Stream_Element_Count := 0 ;
        Csha1 : Gnat.SHA1.Context := Gnat.SHA1.Initial_Context ;
     begin
        ada.Streams.stream_io.Open(f , ada.streams.Stream_IO.In_File , filename ) ;
        while not ada.Streams.Stream_IO.End_Of_File(f)
        loop
           ada.Streams.stream_io.Read(f,buffer,bufbytes) ;
           Gnat.SHA1.Update(Csha1,Buffer(1..Bufbytes)) ;
           bytes := bytes + bufbytes ;
        end loop ;
        ada.Streams.Stream_IO.Close(f) ;
        return Gnat.SHA1.Digest(Csha1) ;
         exception
            when others =>
               put("Unable to generate sha1 digest for ");
               put_line(filename) ;
               return "" ;
     end digest_sha1 ;

     function digest_sha224( filename : string ) return String is
         use Ada.Streams ;
         f : ada.Streams.Stream_IO.File_Type ;
         buffer : ada.Streams.Stream_Element_Array(1..BLOCKSIZE) ;
         bufbytes : ada.Streams.Stream_Element_Count ;
         bytes : ada.Streams.Stream_Element_Count := 0 ;
         Csha224 : Gnat.SHA224.Context := Gnat.SHA224.Initial_Context ;
      begin
         ada.Streams.stream_io.Open(f , ada.streams.Stream_IO.In_File , filename ) ;
         while not ada.Streams.Stream_IO.End_Of_File(f)
         loop
            ada.Streams.stream_io.Read(f,buffer,bufbytes) ;
            Gnat.SHA224.Update(Csha224,Buffer(1..Bufbytes)) ;
            bytes := bytes + bufbytes ;
         end loop ;
         ada.Streams.Stream_IO.Close(f) ;
         return Gnat.SHA224.Digest(Csha224) ;
          exception
             when others =>
                put("Unable to generate sha224 digest for ");
                put_line(filename) ;
                return "" ;
      end digest_sha224 ;

      function digest_sha256( filename : string ) return String is
          use Ada.Streams ;
          f : ada.Streams.Stream_IO.File_Type ;
          buffer : ada.Streams.Stream_Element_Array(1..BLOCKSIZE) ;
          bufbytes : ada.Streams.Stream_Element_Count ;
          bytes : ada.Streams.Stream_Element_Count := 0 ;
          Csha256 : Gnat.SHA256.Context := Gnat.SHA256.Initial_Context ;
       begin
          ada.Streams.stream_io.Open(f , ada.streams.Stream_IO.In_File , filename ) ;
          while not ada.Streams.Stream_IO.End_Of_File(f)
          loop
             ada.Streams.stream_io.Read(f,buffer,bufbytes) ;
             Gnat.SHA256.Update(Csha256,Buffer(1..Bufbytes)) ;
             bytes := bytes + bufbytes ;
          end loop ;
          ada.Streams.Stream_IO.Close(f) ;
          return Gnat.SHA256.Digest(Csha256) ;
           exception
              when others =>
                 put("Unable to generate sha256 digest for ");
                 put_line(filename) ;
                 return "" ;
       end digest_sha256 ;

       function digest_sha384( filename : string ) return String is
           use Ada.Streams ;
           f : ada.Streams.Stream_IO.File_Type ;
           buffer : ada.Streams.Stream_Element_Array(1..BLOCKSIZE) ;
           bufbytes : ada.Streams.Stream_Element_Count ;
           bytes : ada.Streams.Stream_Element_Count := 0 ;
           Csha384 : Gnat.SHA384.Context := Gnat.SHA384.Initial_Context ;
        begin
           ada.Streams.stream_io.Open(f , ada.streams.Stream_IO.In_File , filename ) ;
           while not ada.Streams.Stream_IO.End_Of_File(f)
           loop
              ada.Streams.stream_io.Read(f,buffer,bufbytes) ;
              Gnat.SHA384.Update(Csha384,Buffer(1..Bufbytes)) ;
              bytes := bytes + bufbytes ;
           end loop ;
           ada.Streams.Stream_IO.Close(f) ;
           return Gnat.SHA384.Digest(Csha384) ;
            exception
               when others =>
                  put("Unable to generate sha384 digest for ");
                  put_line(filename) ;
                  return "" ;
        end digest_sha384 ;

        function digest_sha512( filename : string ) return String is
            use Ada.Streams ;
            f : ada.Streams.Stream_IO.File_Type ;
            buffer : ada.Streams.Stream_Element_Array(1..BLOCKSIZE) ;
            bufbytes : ada.Streams.Stream_Element_Count ;
            bytes : ada.Streams.Stream_Element_Count := 0 ;
            Csha512 : Gnat.SHA512.Context := Gnat.SHA512.Initial_Context ;
         begin
            ada.Streams.stream_io.Open(f , ada.streams.Stream_IO.In_File , filename ) ;
            while not ada.Streams.Stream_IO.End_Of_File(f)
            loop
               ada.Streams.stream_io.Read(f,buffer,bufbytes) ;
               Gnat.SHA512.Update(Csha512,Buffer(1..Bufbytes)) ;
               bytes := bytes + bufbytes ;
            end loop ;
            ada.Streams.Stream_IO.Close(f) ;
            return Gnat.SHA512.Digest(Csha512) ;
             exception
                when others =>
                   put("Unable to generate sha512 digest for ");
                   put_line(filename) ;
                   return "" ;
         end digest_sha512 ;

    function digest_sha( filename : string ;
                         level : integer := 1 ) return string is
    begin
        case level is
            when 1 => return digest_sha1( filename );
            when 224 => return digest_sha224( filename );
            when 256 => return digest_sha256( filename );
            when 384 => return digest_sha384( filename );
            when 512 => return digest_sha512( filename );
            when others =>
                 Put_Line("Exception");
                 return "?";
        end case ;
    end digest_sha ;

    procedure digest_sha( context : integer ;
                          filename : string ) is
    begin
        case context is
            when 1 =>   put(digest_sha1( filename ));
            when 224 => put(digest_sha224( filename ));
            when 256 => put(digest_sha256( filename ));
            when 384 => put(digest_sha384( filename ));
            when 512 => put(digest_sha512( filename ));
            when others =>
                 Put_Line("Exception");
        end case ;
        Put(" : ");
        Put_Line(filename);
    end digest_sha ;
    procedure digest_sha( dirname : string ;
                          pattern : string ;
                          level : integer := 1) is
    begin
        dirwalk_sha( level , dirname , pattern , digest_sha'access) ;
    end digest_sha ;
    
   
   function digest_crc( filename : string ) return String is
       use Ada.Streams ;
       f : ada.Streams.Stream_IO.File_Type ;
       buffer : ada.Streams.Stream_Element_Array(1..BLOCKSIZE) ;
       bufbytes : ada.Streams.Stream_Element_Count ;
       bytes : ada.Streams.Stream_Element_Count := 0 ;
      
      C : Gnat.CRC32.CRC32 ;
      V : Interfaces.Unsigned_32 ;
   begin
      GNAT.Crc32.Initialize(c);
      
       ada.Streams.stream_io.Open(f , ada.streams.Stream_IO.In_File , filename ) ;
       while not ada.Streams.Stream_IO.End_Of_File(f)
       loop
          ada.Streams.stream_io.Read(f,buffer,bufbytes) ;
          Gnat.CRC32.Update(C,Buffer(1..Bufbytes)) ;
          bytes := bytes + bufbytes ;
       end loop ;
      ada.Streams.Stream_IO.Close(f) ;
      V := GNAT.CRC32.GET_Value(C) ;
      return Hex.Image(V);
   end digest_crc ;
   
   procedure digest_crc( context : integer ; filename : string ) is
    begin
      put( digest_crc(filename)) ;
      Put( " : ");
      put_line( filename );
   end digest_crc ;
   
   procedure digest_crc( dirname : string ;
                         pattern : string ) is
   begin
        Dirwalk_Crc32( 0 , dirname , pattern , digest_crc'access) ;
   end digest_crc ;
   
end digest_pkg ;
